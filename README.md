This library is intended to interact with button pluged on a ESP8266 using the Arduino SDK

Code Example
```c++
#include <HIB.h>

HIB *button;
HIB *button2;

void onButtonPressed(uint8_t pin) { Serial.printf("Button %d pressed \n", pin); }

void setup()
{
	Serial.begin(115200);
	// put your setup code here, to run once:
	button  = new HIB(0, HIGH, onButtonPressed);
	button2 = new HIB(4, HIGH, onButtonPressed);
}

void loop() {}

```
