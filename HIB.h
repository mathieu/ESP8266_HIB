#pragma once
#include "Arduino.h"
extern "C" {
#include "osapi.h"
}

// Human Interface Button
class HIB {
private:
    ETSTimer timer;
    unsigned long previousMillis;
    unsigned long longPressMsec;
    unsigned long shortPressMsec;
    void onInternalButtonPressed();
    void onInternalButtonReleased();
    void onInternalLongButtonPressed();
    void invertState();
    void (* onButtonPressed)(uint8_t pin, int state);
    void (* onButtonReleased)(uint8_t pin, int state);
    void (* onLongButtonPressed)(uint8_t pin);
    friend void __timerCallback(void *data);
public:
    uint8_t pin;
    uint8_t initialState;
    uint8_t state;
    uint8_t debouncing;
    HIB(uint8_t pin, uint8_t initialState,
        void (* onButtonPressed)(uint8_t pin, int state) = NULL,
        void (* onButtonReleased)(uint8_t pin, int state) = NULL,
        void (* onLongButtonPressed)(uint8_t pin) = NULL,
        unsigned long longPressMsec = 5000, unsigned long shortPressMsec = 50);
    void IRQ_handler();
};
