#include "HIB.h"
#define MAX_PIN 15

HIB *HibList[MAX_PIN+1];
void ICACHE_RAM_ATTR sws_isr_0() { HibList[0]->IRQ_handler(); }
void ICACHE_RAM_ATTR sws_isr_1() { HibList[1]->IRQ_handler(); }
void ICACHE_RAM_ATTR sws_isr_2() { HibList[2]->IRQ_handler(); }
void ICACHE_RAM_ATTR sws_isr_3() { HibList[3]->IRQ_handler(); }
void ICACHE_RAM_ATTR sws_isr_4() { HibList[4]->IRQ_handler(); }
void ICACHE_RAM_ATTR sws_isr_5() { HibList[5]->IRQ_handler(); }
// Pin 6 to 11 can not be used
void ICACHE_RAM_ATTR sws_isr_12() { HibList[12]->IRQ_handler(); }
void ICACHE_RAM_ATTR sws_isr_13() { HibList[13]->IRQ_handler(); }
void ICACHE_RAM_ATTR sws_isr_14() { HibList[14]->IRQ_handler(); }
void ICACHE_RAM_ATTR sws_isr_15() { HibList[15]->IRQ_handler(); }

static void (*ISRList[MAX_PIN+1])() = {
	sws_isr_0,
	sws_isr_1,
	sws_isr_2,
	sws_isr_3,
	sws_isr_4,
	sws_isr_5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	sws_isr_12,
	sws_isr_13,
	sws_isr_14,
	sws_isr_15
};

void __timerCallback(void *data) {
	HIB *hib = static_cast<HIB *>(data);
	hib->debouncing = false;
	hib->invertState();
	if (hib->state != digitalRead(hib->pin)){
		hib->state = !hib->state;
		return;
	}
	if(hib->state != hib->initialState)
		hib->onInternalButtonPressed();
	else
		hib->onInternalButtonReleased();
}


HIB::HIB(uint8_t p, uint8_t initState,
		 void (* userOnButtonPressed)(uint8_t pin, int state),
		 void (* userOnButtonReleased)(uint8_t pin, int state),
		 void (* userOnLongButtonPressed)(uint8_t pin),
		 unsigned long longPress, unsigned long shortPress):
	previousMillis(0),
	longPressMsec(longPress),
	shortPressMsec(shortPress),
	onButtonPressed(userOnButtonPressed),
	onButtonReleased(userOnButtonReleased),
	onLongButtonPressed(userOnLongButtonPressed),
	pin(p), initialState(initState),
	state(initState), debouncing(false) {
		pinMode(pin, INPUT_PULLUP);
		HibList[pin] = this;
		attachInterrupt(digitalPinToInterrupt(pin), ISRList[pin], CHANGE);
		os_timer_setfn(&timer, __timerCallback, this);
	}

void HIB::IRQ_handler(){
	if(!debouncing){
		debouncing = true;
		os_timer_arm(&timer, shortPressMsec, 0);
	}
}


void HIB::onInternalButtonPressed(){
	previousMillis = millis();
	if(onButtonPressed)
		onButtonPressed(pin, state);
}

void HIB::onInternalLongButtonPressed(){
	if(onLongButtonPressed)
		onLongButtonPressed(pin);
}

void HIB::onInternalButtonReleased(){
	if(onButtonReleased)
		onButtonReleased(pin, state);
	if(millis() - previousMillis >= longPressMsec){
		onInternalLongButtonPressed();
	}
}

void HIB::invertState(){
	state = !state;
}
